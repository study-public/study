package com.example.study.stack

import spock.lang.Specification

class StackTest extends Specification {


    def "should push an element into the stack"() {
        given:
        int first = 1;
        int second = 2;
        Stack stack = new Stack();

        when:
        stack.push(1);
        stack.push(2);

        then:
        stack.size() == 2
        stack.isEmpty() == false
    }

    def "should remove element from the stack"() {
        given:
        int first = 1;
        int second = 2;
        Stack stack = new Stack();
        stack.push(first);
        stack.push(second);

        when:
        stack.pop();

        then:
        stack.size() == 1
        stack.isEmpty() == false

    }

    def "should have an empty stack"() {
        given:
        int first = 1;
        int second = 2;
        Stack stack = new Stack();
        stack.push(first);
        stack.push(second);

        when:
        stack.pop();
        stack.pop();

        then:
        stack.size() == 0
        stack.isEmpty() == true
    }
}
