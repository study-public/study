package com.example.study.stack;

public class Stack {

    private int TOP = 0;

    private int elements[] = new int[100];

    public void push(Integer integer) {
        elements[TOP++] = integer;
    }

    public Integer size() {
        return TOP;
    }

    public void pop() {
        if (TOP != 0) {
            elements[TOP--] = 0;
        }
    }

    public boolean isEmpty() {
        return TOP == 0;
    }
}
